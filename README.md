#ShichimiFX
A small lib containing more an more of my helper classes and components.

([Shichimi](http://en.wikipedia.org/wiki/Shichimi) is my favorite Japanese spice mixture)

[ ![Download](https://api.bintray.com/packages/jerady/maven/ShichimiFX/images/download.svg) ](https://bintray.com/jerady/maven/ShichimiFX/_latestVersion)

# Maven / Gradle

**Since Shichimi 1.1.0 I moved it to [Bintray](https://bintray.com/jerady/maven/ShichimiFX), so you have to add the bintray repo to you build file as it is not yet available at Maven Central!**

```
#!xml
<dependency>
  <groupId>de.jensd</groupId>
  <artifactId>shichimifx</artifactId>
  <version>1.2.1</version>
</dependency>
```

#SplitPaneDividerSlider

***de.jensd.shichimifx.utils.SplitPaneDividerSlider***

![2014-10-14 15_50_00-SplitPaneDivierSlider Demo.jpg](https://bitbucket.org/repo/zgMERg/images/3204522187-2014-10-14%2015_50_00-SplitPaneDivierSlider%20Demo.jpg)

Usage:

Create a SplitPaneDividerSlider instance to control the state of the desired SplitPane then e.g. let the state be controlled by a ToggleButton:  

```
#!java

SplitPaneDividerSlider leftSplitPaneDividerSlider = new SplitPaneDividerSlider(centerSplitPane, 0, SplitPaneDividerSlider.Direction.LEFT);

leftToggleButton.selectedProperty().addListener((ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) -> {
            leftSplitPaneDividerSlider.setAimContentVisible(t1);
        });

```

**SplitPaneDivider runnable demo: de.jensd.shichimifx.demo.RunSplitPaneDividerSliderDemo**

[SplitPaneDivider demo clip ](http://youtu.be/RkgVffjJwXw)
[SplitPaneDivider in picmodo](http://youtu.be/NrgW3PMa3YA)


#TabPaneDetacher
***de.jensd.shichimifx.utils.TabPaneDetacher***

Utility to make all Tabs of a TabPane detachable. Basically it should be possible to enable the detach capability of any JavaFX TabPane by just calling:

![2014-10-14 15_50_57-ShichimiFX - NetBeans IDE 8.0.1.jpg](https://bitbucket.org/repo/zgMERg/images/3151432974-2014-10-14%2015_50_57-ShichimiFX%20-%20NetBeans%20IDE%208.0.1.jpg)

```
#!java

public class TabPaneDetacherDemoController {

    @FXML
    private TabPane demoTabPane;
    
    @FXML
    public void initialize() {
        TabPaneDetacher.create().makeTabsDetachable(demoTabPane);
    }    
    
}
```
  
A new window is created containing the content of the tab, the window title is set to the tab name value. On closing that window the tab is restored at the original position:  

**TabPabeDetacher runnable demo: de.jensd.shichimifx.demo.RunTabPaneDetacherDemo**

[TabPabeDetacher demo clip](http://youtu.be/ow54mmBgL7w)


# Indicator

***de.jensd.shichimifx.controls.indicator.Indicator***

A simple control to indicate a status (e.g. Server Connected)

![2014-10-14 15_50_31-Indicator Demo.jpg](https://bitbucket.org/repo/zgMERg/images/2991956895-2014-10-14%2015_50_31-Indicator%20Demo.jpg)

###Usage (code from MQTT.fx Broker-connector)
```
#!java

        ObjectBinding<Indicator.Result> result = new When(model.brokerConnectedProperty()).then(Indicator.Result.PASS).otherwise(Indicator.Result.FAIL);
        connectionStatusIndicator = new Indicator();
        connectionStatusIndicator.setResult(Indicator.Result.FAIL);
        connectionStatusIndicator.setPrefSize(20.0, 20.0);
        connectionStatusIndicator.resultProperty().bind(result);
```

**Indicator runnable demo: de.jensd.shichimifx.demo.RunStatusIndicatorDemo**

#OS

***de.jensd.shichimifx.utils.OS***

A small helper to detect the underlying OS and OS specific App-Data paths.

**public static String getSystemLocalAppDataPath()**

Windows: ("user.home") + "/AppData/Local"

OSX: ("user.home") + "/Library/Application Support/"

Linux: ("user.home")

**public static String getOSName()**

**public static boolean isWindows7()**

**public static boolean isWindows8()**

**public static boolean isWindows()**

**public static boolean isMac()**

**public static boolean isLinux()**


#SimpleInputConstraints#

***de.jensd.shichimifx.utils.SimpleInputConstraints***

Provides some basic input constraints to be used for input controls.

**Usage:**

```
#!java
        SimpleInputConstraints.noBlanks(noBlanksField);
        SimpleInputConstraints.noLeadingBlanks(noLeadingBlanksField);
        SimpleInputConstraints.numbersOnly(numbersOnlyField);
        SimpleInputConstraints.lettersOnly(lettersOnlyField);

        SimpleInputConstraints.noLeadingBlanks(valueComboBox.getEditor());
        SimpleInputConstraints.noLeadingBlanks(noLeadingBlanksComboBox.getEditor());
        SimpleInputConstraints.noBlanks(noBlanksComboBox.getEditor());
        SimpleInputConstraints.numbersOnly(numbersOnlyComboBox.getEditor());
        SimpleInputConstraints.lettersOnly(lettersOnlyComboBox.getEditor());
```

[Blog post (its former name was 'InputConstraints')](http://www.jensd.de/wordpress/?p=1408)

#MouseRobot

***de.jensd.shichimifx.utils.MouseRobot***

Helper to get the current mouse position (I use it e.g. für drag and drop operations)

![2014-10-14 15_49_37-MouseRobot Demo.jpg](https://bitbucket.org/repo/zgMERg/images/2594668401-2014-10-14%2015_49_37-MouseRobot%20Demo.jpg)

**MouseRobot runnable demo: de.jensd.shichimifx.demo.RunMouseRobotDemo**

#Shell
***de.jensd.shichimifx.utils.Shell***

Helper to execute execute commands via the system shell.

**Usage:** see [Blog post](http://wp.me/p38FCL-sz)

# Desktop
***de.jensd.shichimifx.utils.Desktop***

Extension of java.awt.Desktop to get rid of it's issues to open/edit files on Mac OS, Windows and Linux.

**Usage:** see [Blog post](http://wp.me/p38FCL-sz)


# Console
##ConsoleStream, ConsoleDude
***de.jensd.shichimifx.utils.ConsoleStream***

***de.jensd.shichimifx.utils.ConsoleDude***

![Console Demo.png](https://bitbucket.org/repo/zgMERg/images/3416197909-Console%20Demo.png)

ConsoleStream is a merge of an OuputStream and a TextArea. It can be used to "pipe" the stream to the given TextArea, e.g for a log view console or a System.out console.

**Usage:** see [Blog post](http://wp.me/p38FCL-sr)

#Update UI for Refuel#
***de.jensd.shichimifx.refuel.FileDownloadService***

***de.jensd.shichimifx.refuel.UpdatePane***

***de.jensd.shichimifx.util.OptionCheckBoxDialog***

**"Refuel-Framework"** see [github repo](https://github.com/dimaki/refuel) from Dino Tsoumakis.

![updates-available.jpg](https://bitbucket.org/repo/zgMERg/images/1513215118-updates-available.jpg)

![updates-load.jpg](https://bitbucket.org/repo/zgMERg/images/1274267306-updates-load.jpg)



#License
ShichimiFX is licensed under the [Apache 2.0 license][1].
If this license is not suitable, please contact me to discuss an alternative license.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
package de.jensd.shichimifx.demo.util;

import java.util.Random;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.util.Duration;

/**
 *
 * @author Jens Deters
 */
public class TextProducer extends Service<Void> {

    private Timeline multilineTimeline;
    private final Random rand = new Random();

    public final static String[] LOREM_IPSUM = {"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n",
        "At vero eos et accusam et justo duo dolores et ea rebum.\n",
        "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n",
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n",
        "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n"};

    @Override
    protected Task<Void> createTask() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                multilineTimeline = new Timeline(
                        new KeyFrame(Duration.millis(100),
                                ae -> {
                                    updateMessage(nextRandomLine());
                                }));
                multilineTimeline.setCycleCount(Animation.INDEFINITE);
                multilineTimeline.play();
                while (true) {
                    if (isCancelled()) {
                        if (multilineTimeline != null) {
                            multilineTimeline.stop();
                        }
                        break;
                    }
                }
                return null;
            }
        };
    }

    public String nextRandomLine() {
        return LOREM_IPSUM[rand.nextInt(LOREM_IPSUM.length - 1)];
    }

}

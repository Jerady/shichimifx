/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.demo.controller;

import de.jensd.shichimifx.utils.TitledPaneDude;
import javafx.fxml.FXML;
import javafx.scene.control.TitledPane;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 27-10-2014
 */
public class TitledPaneDemoController{

    @FXML
    private TitledPane titledPane1;

    @FXML
    public void initialize() {
        TitledPaneDude.makeMac(titledPane1);
        
    }
}

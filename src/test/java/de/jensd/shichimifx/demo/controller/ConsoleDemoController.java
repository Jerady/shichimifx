package de.jensd.shichimifx.demo.controller;

import de.jensd.shichimifx.utils.ConsoleDude;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 *
 * @author Jens Deters
 */
public class ConsoleDemoController {

    private final static Logger LOGGER = Logger.getLogger(ConsoleDemoController.class.getSimpleName());

    private final static String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n"
            + "At vero eos et accusam et justo duo dolores et ea rebum.\n\n"
            + "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n"
            + "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.\n"
            + "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\n";

    @FXML
    private TextArea logMessageArea;

    @FXML
    private TextArea stdOutMessageArea;

    @FXML
    private Button hookButton;

    @FXML
    private Button releaseButton;

    public void initialize() {
        stdOutMessageArea.getStyleClass().add("console");
        ConsoleDude.stdStreamsHookedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (newValue) {
                stdOutMessageArea.getStyleClass().add("console-hooked");
            } else {
                stdOutMessageArea.getStyleClass().remove("console-hooked");
            }

        });
        LOGGER.addHandler(ConsoleDude.createConsoleStreamHandler(logMessageArea));
        hookButton.disableProperty().bind(ConsoleDude.stdStreamsHookedProperty());
        releaseButton.disableProperty().bind(ConsoleDude.stdStreamsHookedProperty().not());
    }

    @FXML
    public void hookStdOut() {
        LOGGER.info("Hook System.out + System.err");
        ConsoleDude.hookStdStreams(stdOutMessageArea);
    }

    @FXML
    public void releaseStdOut() {
        LOGGER.info("Release System.out + System.err");
        ConsoleDude.restoreStdStreams();
    }

    @FXML
    public void printDemoContent() {
        LOGGER.info("Print demo content");
        new Thread(() -> {
            for (int i = 0; i < 20000; i++) {
                System.out.println(LOREM_IPSUM);
            }
        }).start();

    }

}

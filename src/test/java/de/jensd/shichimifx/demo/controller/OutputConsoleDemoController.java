package de.jensd.shichimifx.demo.controller;

import de.jensd.shichimifx.control.OutputConsole;
import de.jensd.shichimifx.demo.util.TextProducer;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters
 */
public class OutputConsoleDemoController {

    private final static Logger LOGGER = Logger.getLogger(OutputConsoleDemoController.class.getSimpleName());

    @FXML
    private Button startButton;

    @FXML
    private Button stopButton;

    @FXML
    private Label charsCountLabel;
    @FXML
    private Label linesCountLabel;
    @FXML
    private Label maxCharsLabel;

    private OutputConsole outputConsole;

    @FXML
    private ProgressIndicator textProducerProgressIndicator;

    @FXML
    private Slider maxLengthSlider;
    
    @FXML
    private VBox rootBox;

    private TextProducer textProducer;

    final int MAX_LINES = 100;

    public void initialize() {
        outputConsole = new OutputConsole(MAX_LINES);
        VBox.setVgrow(outputConsole, Priority.ALWAYS);
        rootBox.getChildren().add(outputConsole);
        
        textProducer = new TextProducer();
        startButton.disableProperty().bind(textProducer.runningProperty());
        stopButton.disableProperty().bind(textProducer.runningProperty().not());
        textProducerProgressIndicator.visibleProperty().bind(textProducer.runningProperty());
        textProducer.messageProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            outputConsole.appendText(newValue);
        });

        outputConsole.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            charsCountLabel.setText(String.valueOf(outputConsole.length()));
            linesCountLabel.setText(String.valueOf(outputConsole.lineCount()));
        });
        
       outputConsole.maxLengthProperty().bindBidirectional(maxLengthSlider.valueProperty());
       maxCharsLabel.textProperty().bind(outputConsole.maxLengthProperty().asString());
    }

    @FXML
    public void onStart() {
        LOGGER.info("Start");
        if (!textProducer.isRunning()) {
            textProducer.restart();
        }
    }

    @FXML
    public void onStop() {
        LOGGER.info("Stop");
        if (textProducer.isRunning()) {
            textProducer.cancel();
        }
    }

    @FXML
    public void onClear() {
        LOGGER.info("clear");
        outputConsole.clear();
    }
    
    @FXML
    public void onAppendText(){
        outputConsole.appendText(textProducer.nextRandomLine());
    }
}

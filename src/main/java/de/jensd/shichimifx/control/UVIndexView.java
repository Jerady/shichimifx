/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters
 */
public class UVIndexView extends Control{

    private IntegerProperty value;
    

    public UVIndexView(){
        getStyleClass().addAll("uvindex-view");
    }
    
    public IntegerProperty valueProperty() {
        if(value == null){
            value = new SimpleIntegerProperty();
        }
        return value;
    }

    
    public Integer getValue() {
        return valueProperty().get();
    }

    public void setValue(Integer value) {
        this.valueProperty().set(value);
    }
    
     @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }
    
    
}

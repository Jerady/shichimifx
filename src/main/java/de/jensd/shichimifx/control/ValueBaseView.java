/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class ValueBaseView extends Control {

    private StringProperty title;
    private ObjectProperty<Pos> titleAlignment;

    public ValueBaseView() {
        getStyleClass().addAll("root", "value-base-view");
        setTitle("");
    }

    public ObjectProperty<Pos> titleAlignmentProperty() {
        if (titleAlignment == null) {
            titleAlignment = new SimpleObjectProperty<>(Pos.CENTER_LEFT);
        }
        return titleAlignment;
    }

    public Pos getTitleAlignment() {
        return titleAlignmentProperty().getValue();
    }

    public void setTitleAlignment(Pos titlePos) {
        titleAlignmentProperty().setValue(titlePos);
    }

    public final StringProperty titleProperty() {
        if (title == null) {
            title = new SimpleStringProperty("");
        }
        return title;
    }

    public final String getTitle() {
        return titleProperty().getValue();
    }

    public final void setTitle(String value) {
        titleProperty().setValue(value);
    }

    public final ObjectProperty<Node> graphicProperty() {
        if (graphic == null) {
            graphic = new SimpleObjectProperty<>();
        }
        return graphic;
    }
    private ObjectProperty<Node> graphic;

    public final void setGraphic(Node value) {
        graphicProperty().setValue(value);
    }

    public final Node getGraphic() {
        return graphic == null ? null : graphic.getValue();
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource("valuebaseview.css").toExternalForm();
    }

}

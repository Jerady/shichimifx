/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control;

import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class IconValueUnitView extends ValueBaseView {

    private StringProperty unit;
    private StringProperty glyphName;
    private StringProperty fontSize;
    private StringProperty value;
    private ObjectProperty<Number> glyphSize;
    private ObjectProperty<Pos> valueAlignment;

    public IconValueUnitView() {
        getStyleClass().addAll("root", "value-view");
        setValue("0.0");
        setUnit("\u2103");
        setFontSize("1em");
        setGlyphSize(48.0);
        setTitle("EMPTY");
        setGlyphName(WeatherIcon.THERMOMETER.name());
    }

    public ObjectProperty<Pos> valueAlignmentProperty() {
        if (valueAlignment == null) {
            valueAlignment = new SimpleObjectProperty<>(Pos.CENTER_RIGHT);
        }
        return valueAlignment;
    }

    public Pos getValueAlignment() {
        return valueAlignmentProperty().getValue();
    }

    public void setValueAlignment(Pos valuePos) {
        valueAlignmentProperty().setValue(valuePos);
    }

    public final StringProperty unitProperty() {
        if (unit == null) {
            unit = new SimpleStringProperty();
        }
        return unit;
    }

    public final String getUnit() {
        return unitProperty().getValue();
    }

    public final void setUnit(String unit) {
        unitProperty().setValue(unit);
    }

    public final StringProperty glyphNameProperty() {
        if (glyphName == null) {
            glyphName = new SimpleStringProperty();
        }
        return glyphName;
    }

    public final String getGlyphName() {
        return glyphNameProperty().getValue();
    }

    public final void setGlyphName(String glyphName) {
        glyphNameProperty().setValue(glyphName);
    }

    public final ObjectProperty<Number> glyphSizeProperty() {
        if (glyphSize == null) {
            glyphSize = new SimpleObjectProperty();
        }
        return glyphSize;
    }

    public final Number getGlyphSize() {
        return glyphSizeProperty().getValue();
    }

    public final void setGlyphSize(Number glyphSize) {
        glyphSizeProperty().setValue(glyphSize);
    }

    public final StringProperty valueProperty() {
        if (value == null) {
            value = new SimpleStringProperty("");
        }
        return value;
    }

    public final String getValue() {
        return valueProperty().getValue();
    }

    public final void setValue(String value) {
        valueProperty().setValue(value);
    }

    public final StringProperty fontSizeProperty() {
        if (fontSize == null) {
            fontSize = new SimpleStringProperty();
        }
        return fontSize;
    }

    public final String getFontSize() {
        return fontSizeProperty().getValue();
    }

    public final void setFontSize(String fontSize) {
        fontSizeProperty().setValue(fontSize);
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }

}

/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control.skin;

import de.jensd.shichimifx.control.ValueBaseView;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class ValueBaseViewSkin extends SkinBase<ValueBaseView> {

    private static final double PREFERRED_WIDTH = 300;
    private static final double PREFERRED_HEIGHT = 100;

    private Label titleLabel;
    private Separator titleSeparator;
    private HBox titleBox;
    private HBox centerBox;
    private VBox mainBox;

    public ValueBaseViewSkin(ValueBaseView control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
        getSkinnable().setPrefHeight(PREFERRED_HEIGHT);
    }

    private void initNodes() {
        titleLabel = new Label("TITLE");
        titleBox = new HBox();
        titleBox.getChildren().add(titleLabel);
        titleSeparator = new Separator();
        centerBox = new HBox();
        HBox.setHgrow(centerBox, Priority.ALWAYS);
        mainBox = new VBox();
        VBox.setVgrow(mainBox, Priority.ALWAYS);
        mainBox.getChildren().addAll(titleBox, titleSeparator, centerBox);
        getChildren().add(mainBox);
    }

    private void initStyleClasses() {
        titleLabel.getStyleClass().add("title");
        titleBox.getStyleClass().add("title-box");
    }

    private void registerBindings() {
        titleLabel.textProperty().bind(getSkinnable().titleProperty());
        titleBox.alignmentProperty().bind(getSkinnable().titleAlignmentProperty());
    }

}

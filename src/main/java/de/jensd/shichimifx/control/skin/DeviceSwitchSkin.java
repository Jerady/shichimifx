/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control.skin;

import de.jensd.shichimifx.control.DeviceSwitch;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class DeviceSwitchSkin extends SkinBase<DeviceSwitch> {

    private static final double PREFERRED_WIDTH = 300;
    private Label titleLabel;
    private Button onButton;
    private Button offButton;
    private HBox mainPane;

    public DeviceSwitchSkin(DeviceSwitch control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
    }
    

    private void initNodes() {
        titleLabel = new Label();
        onButton = new Button();
        offButton = new Button();
        Region spring = new Region();
        HBox.setHgrow(spring, Priority.ALWAYS);
        mainPane = new HBox();
        mainPane.setSpacing(0.0);
        mainPane.setAlignment(Pos.CENTER_LEFT);
        mainPane.getChildren().addAll(titleLabel, spring, onButton, offButton);
        getChildren().add(mainPane);
    }

    private void initStyleClasses() {
        titleLabel.getStyleClass().add("title");
        onButton.getStyleClass().add("on");
        offButton.getStyleClass().add("off");
    }
    
    private void registerBindings() {
        titleLabel.textProperty().bind(getSkinnable().titleProperty());
        onButton.textProperty().bind(getSkinnable().onButtonTextProperty());
        offButton.textProperty().bind(getSkinnable().offButtonTextProperty());
        onButton.onActionProperty().bind(getSkinnable().onSwitchOnProperty());
        offButton.onActionProperty().bind(getSkinnable().onSwitchOffProperty());
    }

}

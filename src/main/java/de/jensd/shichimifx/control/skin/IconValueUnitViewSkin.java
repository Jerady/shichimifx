package de.jensd.shichimifx.control.skin;

import de.jensd.shichimifx.control.IconValueUnitView;
import de.jensd.fx.glyphs.weathericons.WeatherIconView;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class IconValueUnitViewSkin extends SkinBase<IconValueUnitView> {

    private static final double PREFERRED_WIDTH = 300;
    private static final double PREFERRED_HEIGHT = 100;

    private Label valueLabel;
    private Label unitLabel;
    private WeatherIconView glyph;
    private VBox iconBox;
    private HBox valueBox;
    private HBox centerBox;
    private VBox unitBox;
    private VBox mainBox;

    public IconValueUnitViewSkin(IconValueUnitView control) {
        super(control);
        init();
        initNodes();
        initStyleClasses();
        registerBindings();
    }

    private void init() {
        getSkinnable().setPrefWidth(PREFERRED_WIDTH);
        getSkinnable().setPrefHeight(PREFERRED_HEIGHT);
    }

    private void initNodes() {
        glyph = new WeatherIconView();
        valueLabel = new Label("VALUE");
        unitLabel = new Label("UNIT");
        iconBox = new VBox();
        iconBox.getChildren().addAll(glyph);
        iconBox.setAlignment(Pos.CENTER);
        VBox.setVgrow(iconBox, Priority.NEVER);
        valueBox = new HBox();
        HBox.setHgrow(valueBox, Priority.ALWAYS);
        valueBox.getChildren().addAll(valueLabel);
        unitBox = new VBox();
        unitBox.getChildren().addAll(unitLabel);
        unitBox.setAlignment(Pos.CENTER);
        VBox.setVgrow(unitBox, Priority.NEVER);
        centerBox = new HBox();
        centerBox.setAlignment(Pos.CENTER_RIGHT);
        centerBox.getChildren().addAll(iconBox, valueBox, unitBox);
        HBox.setHgrow(centerBox, Priority.ALWAYS);
        mainBox = new VBox();
        VBox.setVgrow(mainBox, Priority.ALWAYS);
       // getChildren().add(mainBox);
        adjustFonts();
    }

    private void initStyleClasses() {
        glyph.getStyleClass().add("icon");
        valueLabel.getStyleClass().add("value");
        unitLabel.getStyleClass().add("unit");
        iconBox.getStyleClass().add("icon-box");
        valueBox.getStyleClass().add("value-box");
        unitBox.getStyleClass().add("unit-box");
    }

    private void registerBindings() {
        glyph.glyphNameProperty().bind(getSkinnable().glyphNameProperty());
        glyph.glyphSizeProperty().bind(getSkinnable().glyphSizeProperty());
        valueLabel.textProperty().bind(getSkinnable().valueProperty());
        unitLabel.textProperty().bind(getSkinnable().unitProperty());
        valueBox.alignmentProperty().bind(getSkinnable().valueAlignmentProperty());
    }

    private double getEmValue(String em) {
        return Double.parseDouble(em.replace("em", ""));
    }

    private void adjustFonts() {
        String fontSize = getSkinnable().getFontSize();
        if (fontSize.endsWith("em")) {
            double s = getEmValue(fontSize);
            valueLabel.setStyle("-fx-font-size: " + (s * 1.4) + "em;");
            unitLabel.setStyle("-fx-font-size: " + (s * 0.8) + "em;");
        }
    }

}

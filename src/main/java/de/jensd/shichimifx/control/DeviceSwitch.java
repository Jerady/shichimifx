/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.control;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class DeviceSwitch extends Control {

    private StringProperty title;
    private StringProperty onButtonText;
    private StringProperty offButtonText;
    private ObjectProperty<EventHandler<ActionEvent>> onSwitchOn;
    private ObjectProperty<EventHandler<ActionEvent>> onSwitchOff;

    public DeviceSwitch() {
        getStyleClass().addAll("device-switch");
    }

    public final StringProperty titleProperty() {
        if (title == null) {
            title = new SimpleStringProperty("");
        }
        return title;
    }

    public final String getTitle() {
        return titleProperty().getValue();
    }

    public final void setTitle(String value) {
        titleProperty().setValue(value);
    }

    public final StringProperty onButtonTextProperty() {
        if (onButtonText == null) {
            onButtonText = new SimpleStringProperty("ON");
        }
        return onButtonText;
    }

    public final String getOnButtonText() {
        return onButtonTextProperty().getValue();
    }

    public final void setOnButtonText(String value) {
        onButtonTextProperty().setValue(value);
    }

    public final StringProperty offButtonTextProperty() {
        if (offButtonText == null) {
            offButtonText = new SimpleStringProperty("OFF");
        }
        return offButtonText;
    }

    public final String getOffButtonText() {
        return offButtonTextProperty().getValue();
    }

    public final void setOffButtonText(String value) {
        offButtonTextProperty().setValue(value);
    }

    @Override
    public String getUserAgentStylesheet() {
        return getClass().getResource(getClass().getSimpleName().toLowerCase() + ".css").toExternalForm();
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onSwitchOnProperty() {
        if (onSwitchOn == null) {
            onSwitchOn = new SimpleObjectProperty<>();
        }
        return onSwitchOn;
    }

    public final void setOnSwitchOn(EventHandler<ActionEvent> value) {
        onSwitchOnProperty().set(value);
    }

    public final EventHandler<ActionEvent> getOnSwitchOn() {
        return onSwitchOnProperty().get();
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onSwitchOffProperty() {
        if (onSwitchOff == null) {
            onSwitchOff = new SimpleObjectProperty<>();
        }
        return onSwitchOff;
    }

    public final void setOnSwitchOff(EventHandler<ActionEvent> value) {
        onSwitchOffProperty().set(value);
    }

    public final EventHandler<ActionEvent> getOnSwitchOff() {
        return onSwitchOffProperty().get();
    }

}

package de.jensd.shichimifx.control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TextArea;

/**
 *
 * @author Jens Deters
 */
public class OutputConsole extends TextArea {

    private IntegerProperty maxLength;
    public static final int DEFAULT_MAX_LENGTH = 100;

    public OutputConsole() {
        this(DEFAULT_MAX_LENGTH);
    }

    public OutputConsole(final int maxLength) {
        setMaxLength(maxLength);
    }

    public IntegerProperty maxLengthProperty() {
        if (maxLength == null) {
            maxLength = new SimpleIntegerProperty(DEFAULT_MAX_LENGTH);
        }
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        maxLengthProperty().set(maxLength);
    }

    public Integer getMaxLength() {
        return maxLengthProperty().getValue();
    }

    public int length() {
        return getText().length();
    }

    public int lineCount() {
        return getText().split(System.getProperty("line.separator")).length;
    }

    @Override
    public void appendText(String text) {
        if (getMaxLength() < 1) {
            clear();
            return;
        } else {
            int newLength = text.length();
            if (newLength > getMaxLength()) {
                text = text.substring(newLength - getMaxLength() + 1, newLength);
                newLength = text.length();
            }
            if (length() + newLength > getMaxLength()) {
                int deleteIndex = length() + newLength - getMaxLength();
                deleteText(0, deleteIndex);
            }
        }
        super.appendText(text);
    }

}

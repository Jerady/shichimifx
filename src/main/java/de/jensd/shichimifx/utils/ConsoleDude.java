/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import java.io.PrintStream;
import java.util.logging.Formatter;
import java.util.logging.SimpleFormatter;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TextInputControl;

/**
 * Helper for some stream operations.
 *
 * @author Jens Deters
 * @version 1.0.1
 * @since 19-11-2014
 */
public class ConsoleDude {

    public final static PrintStream STD_OUT = System.out;
    public final static PrintStream STD_ERR = System.err;
    private static BooleanProperty stdStreamsHooked = new SimpleBooleanProperty();

    public static void hookStdStreams(TextInputControl outputArea) {
        PrintStream psOut = new PrintStream(new ConsoleStream(outputArea, System.out), true);
        System.setOut(psOut);
        PrintStream psErr = new PrintStream(new ConsoleStream(outputArea, System.err), true);
        System.setOut(psErr);
        stdStreamsHookedProperty().set(true);
    }

    public static void restoreStdStreams() {
        System.setOut(STD_OUT);
        System.setErr(STD_ERR);
        stdStreamsHookedProperty().set(false);
    }

    public static BooleanProperty stdStreamsHookedProperty() {
        if (stdStreamsHooked == null) {
            stdStreamsHooked = new SimpleBooleanProperty();
        }
        return stdStreamsHooked;
    }

    public boolean isHooked() {
        return stdStreamsHookedProperty().get();
    }

    public static ConsoleStreamHandler createConsoleStreamHandler(TextInputControl outputArea) {
        return createConsoleStreamHandler(outputArea, new SimpleFormatter());
    }

    public static ConsoleStreamHandler createConsoleStreamHandler(TextInputControl outputArea, Formatter formatter) {
        return new ConsoleStreamHandler(new ConsoleStream(outputArea), formatter);
    }

}

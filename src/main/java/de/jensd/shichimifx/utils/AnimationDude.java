/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import javafx.animation.FadeTransition;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Provides method to add animation/transition behavoir to controls.
 * 
 * 
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 12-11-2014
 */
public class AnimationDude {
    
    public final static void  addInOutFadeTransition(Duration duration, Node node) {
        addInOutFadeTransition(duration, node, node);
    }

    public final static void  addInOutFadeTransition(Duration duration, Node nodeToFade, Node trigger) {
        nodeToFade.setOpacity(0.0);
        final FadeTransition fadeOutTransition = new FadeTransition(duration, nodeToFade);
        fadeOutTransition.setFromValue(1.0);
        fadeOutTransition.setToValue(0.0);
        final FadeTransition fadeInTransition = new FadeTransition(duration, nodeToFade);
        fadeInTransition.setFromValue(0.0);
        fadeInTransition.setToValue(1.0);
        trigger.setOnMouseEntered(e->{
                    fadeOutTransition.stop();
                    fadeInTransition.play();
        });
        trigger.setOnMouseExited(e->{
                    fadeInTransition.stop();
                    fadeOutTransition.play();
        });
    }

   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.shichimifx.utils;

import java.io.OutputStream;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * Extension of java.util.logging.StreamHandler to support flush() after publish().
 * 
 * @author Jens Deters
 * @version 1.0.1
 * @since 19-11-2014
 */
public class ConsoleStreamHandler extends StreamHandler{

    public ConsoleStreamHandler(OutputStream out, Formatter formatter) {
        super(out, formatter);
    }

    @Override
    public synchronized void publish(LogRecord record) {
        super.publish(record);
        flush();
    }
    
}

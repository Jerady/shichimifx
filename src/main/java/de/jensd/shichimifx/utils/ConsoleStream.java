/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import javafx.application.Platform;
import javafx.scene.control.TextInputControl;

/**
 * ConsoleStream is a merge of an OuputStream and a TextArea. It can be used to
 * "pipe" the stream to the given TextArea, e.g for a log view console or a
 * System.out console.
 *
 * Example:
 *
 * <pre>
 *
 * private void init(){
 *   stdOut = System.out;
 *   stdErr = System.err;
 * }
 *
 * public static void hookStdStreams(TextInputControl outputArea) {
 *   PrintStream psOut = new PrintStream(new ConsoleStream(outputArea, System.out), true);
 *   System.setOut(psOut);
 *   PrintStream psErr = new PrintStream(new ConsoleStream(outputArea, System.err), true);
 *   System.setOut(psErr);
 *   stdStreamsHookedProperty().set(true);
 * }
 *
 * public static void restoreStdStreams() {
 *   System.setOut(STD_OUT);
 *   System.setErr(STD_ERR);
 *   stdStreamsHookedProperty().set(false);
 * }
 *
 * </pre>
 *
 * Or: use ConsoleDude comfort helper
 *
 * @author Jens Deters
 * @version 1.0.1
 * @since 19-11-2014
 */
public class ConsoleStream extends ByteArrayOutputStream {

    private final TextInputControl textOutputComponent;
    private final StringBuilder buffer;
    private final String EOL = System.getProperty("line.separator");
    private final PrintStream printStream;

    public ConsoleStream(TextInputControl ta) {
        this(ta, null);
    }

    public ConsoleStream(TextInputControl ta, PrintStream printStream) {
        this.textOutputComponent = ta;
        this.printStream = printStream;
        buffer = new StringBuilder(80);
    }

    @Override
    public void flush() throws IOException {
        String text = toString();
        if (text.length() == 0) {
            return;
        }
        append(text);
        reset();
    }

    private void append(String text) {
        if (textOutputComponent.getLength() == 0) {
            buffer.setLength(0);
        }
        if (EOL.equals(text)) {
            buffer.append(text);
        } else {
            buffer.append(text);
            clearBuffer();
        }
    }

    private void clearBuffer() {
        String line = buffer.toString();
        Platform.runLater(() -> {
            textOutputComponent.appendText(line);
        });
        if (printStream != null) {
            printStream.print(line);
        }
        buffer.setLength(0);
    }

}

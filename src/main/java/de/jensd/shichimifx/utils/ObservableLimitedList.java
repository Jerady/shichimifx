package de.jensd.shichimifx.utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import javafx.util.Callback;

/**
 *
 * @author Jens Deters
 * @param <T> The type of List
 */
public class ObservableLimitedList<T> extends ModifiableObservableListBase<T> {

    private final ObservableList<WeakReference<T>> list;
    private final int maxSize;
    private IntegerProperty size;
    private IntegerProperty troughput;

    public ObservableLimitedList(int maxSize) {
        this.maxSize = maxSize;
        list = FXCollections.observableList(new ArrayList<WeakReference<T>>());
        list.addListener((ListChangeListener.Change<? extends WeakReference<T>> c) -> {
            updateSizeProperty();
        });
    }

    public ObservableLimitedList(int maxSize, Callback<WeakReference<T>, Observable[]> extractor) {
        this.maxSize = maxSize;
        list = FXCollections.observableList(new ArrayList<WeakReference<T>>(), extractor);
        list.addListener((ListChangeListener.Change<? extends WeakReference<T>> c) -> {
            updateSizeProperty();
        });
    }

    @Override
    public boolean add(T element) {
        boolean result = super.add(element);
        if (size() > maxSize) {
            remove(0);
        }
        return result;
    }

    // delegate overrides:
    @Override
    public T get(int index) {
        return list.get(index).get();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    protected void doAdd(int index, T element) {
        list.add(index, new WeakReference<>(element));
        addTroughput();
    }

    @Override
    protected T doSet(int index, T element) {
        addTroughput();
        return list.set(index, new WeakReference<>(element)).get();
    }

    @Override
    protected T doRemove(int index) {
        return list.remove(index).get();
    }

    public IntegerProperty sizeProperty() {
        if (size == null) {
            size = new SimpleIntegerProperty(0);
        }
        return size;
    }

    public Integer getSize() {
        return sizeProperty().get();
    }

    private void updateSizeProperty() {
        sizeProperty().set(list.size());
    }

    public IntegerProperty troughputProperty() {
        if (troughput == null) {
            troughput = new SimpleIntegerProperty(0);
        }
        return troughput;
    }

    public Integer getTroughput() {
        return troughputProperty().get();
    }

    private void addTroughput() {
        troughputProperty().set(getTroughput() + 1);
    }

    @Override
    public void clear() {
        troughputProperty().set(0);
        super.clear(); 
    }
    

}

/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * A small util class to execute commands via the system shell.
 *
 * @author Jens Deters
 * @version 1.0.4
 * @since 07-01-2015
 */
public class Shell {

    /**
     * Method to check wether a command can be executed by the shell.
     *
     *
     * @param command The command to be executed.
     * @return Returns true if the comamnd has been successfully executed.
     */
    public static boolean canOpen(String... command) {
        try {
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.directory(new File(System.getProperty("user.home"))).start();
            if (process == null) {
                return false;
            }
            try {
                process.exitValue();
                // if this does not throw an IllegalThreadStateException the process is not running/has exited immediately
                return false;
            } catch (IllegalThreadStateException e) {
                // the process seem to be in a running state
                return true;
            }
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * Prepares a command line for execution to open a file via system shell.
     *
     * @param command The command to execute
     * @param fileToOpen The File to open
     * @return Returns a List of parts of the command line. 
     */
    public static List<String> prepareOpenFileLine(String command, String fileToOpen) {
        List<String> commandList = new ArrayList<>();
        for (String s : command.split(" ")) {
            commandList.add(s.trim());
        }
        commandList.add(fileToOpen);
        return commandList;
    }

    public static Optional<Process> execute(List<String> command) throws IOException {
        return execute(new File(System.getProperty("user.home")), command);
    }

    public static Optional<Process> execute(File directory, String... command) throws IOException {
        return execute(directory, Arrays.asList(command));
    }

    public static Optional<Process> execute(String... command) throws IOException {
        return Shell.execute(new File(System.getProperty("user.home")), command);
    }

    public static Optional<Process> execute(File directory, List<String> command) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.directory(directory).start();
        return Optional.of(process);
    }

}

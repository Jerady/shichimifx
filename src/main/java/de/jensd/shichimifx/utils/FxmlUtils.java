/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 * @version 1.0.0
 * @since 23-01-2015
 */
public class FxmlUtils {

    /**
     * Loads the FXML.
     * 
     * @param fxmlURL The URL of FXML to load.
     * @param resources The ResourceBundle to pass to the pane/controller.
     * @return The loaded and created Pane.
     * @throws IOException Thrown as the given URL is not valid.
     */
    public static Pane loadFxmlPane(URL fxmlURL, ResourceBundle resources) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL, resources);
        fxmlLoader.load();
        return fxmlLoader.getRoot();
    }

    /**
     * Generates a URL based in the given FXML file name. Assuming the file is located at "user.home".
     * 
     * @param customFxmlName The name of the FMML file to load.
     * @return The URL to the given FXML file.
     * @throws MalformedURLException Thrown as the given name or URL is not valid.
     */
    public static Optional<URL> getFxmlUrl(String customFxmlName) throws MalformedURLException {
        Path customFxmlPath = Paths.get(System.getProperty("user.home") + "/" + customFxmlName);
        return getFxmlUrl(customFxmlPath);
    }

    /**
     * Generates a URL based in the given FXML file name. Assuming the file is located at "user.home".
     * 
     * @param customFxmlPath The Path to the FMML file to load.
     * @return The URL to the given FXML file.
     * @throws MalformedURLException Thrown as the given name or URL is not valid.
     */
    public static Optional<URL> getFxmlUrl(Path customFxmlPath) throws MalformedURLException {
        URL fxmlURL = null;
        if (Files.exists(customFxmlPath)) {
            fxmlURL = customFxmlPath.toUri().toURL();
        }
        return Optional.ofNullable(fxmlURL);
    }
}

/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

/**
 * Extension of java.awt.Desktop to get rid of it's issues to open/edit files
 * on Mac OS, Windows and Linux.
 *
 * @author Jens Deters
 * @version 1.0.4
 * @since 07-01-2015
 */
public class Desktop {

    /**
     * Opens the given file for editing with the default underlying System
     * settings.
     *
     * @param fileToEdit The file to edit
     * @return Returns true if opening editor was successful.
     */
    public static boolean edit(String fileToEdit) {
        return openWithSystem(fileToEdit);
    }
   
    private static boolean openWithSystem(String fileToOpen) {
        if (OS.isLinux()) {
            if (Shell.canOpen("gnome-open", fileToOpen)) {
                return true;
            } else if (Shell.canOpen("kde-open", fileToOpen)) {
                return true;
            } else if (Shell.canOpen("xdg-open", fileToOpen)) {
                return true;
            }
        } else if (OS.isWindows()) {
            if (Shell.canOpen("explorer", fileToOpen)) {
                return true;
            }
        } else if (OS.isMac()) {
            if (Shell.canOpen("open", fileToOpen)) {
                return true;
            }
        }
        return false;
    }

    

}

/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.utils;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;

/**
 *
 * @author Jens Deters
 * @version 1.0.0
 * @since 27-10-2014
 */
public class TitledPaneDude {

    public static void makeMac(TitledPane titledPaneToDecorate) {
        Label toggleLabel = new Label("ausblenden");
        toggleLabel.setVisible(false);
        toggleLabel.getStyleClass().add("navivigation-titled-pane-toggle-label");
        toggleLabel.setOnMouseEntered(e -> {toggleLabel.setVisible(true);});
        toggleLabel.setOnMouseExited(e -> {toggleLabel.setVisible(false);});
        
        
        
        titledPaneToDecorate.getStyleClass().add("navivigation-titled-pane");
        titledPaneToDecorate.setText(titledPaneToDecorate.getText().toUpperCase());
        titledPaneToDecorate.setGraphic(new Label("ausblenden"));
        titledPaneToDecorate.setContentDisplay(ContentDisplay.RIGHT);
    }

}

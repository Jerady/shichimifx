package de.jensd.shichimifx.dialog;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jens Deters (jens.deters@codecentric.de)
 */
public class Dialogs {
    
    public static void showException(Throwable throwable){
        new ExceptionDialog(throwable).showAndWait();
    }
    
}

/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.dialog;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters (jmail@jensd.de)
 */
public class ExceptionDialog extends Alert {

    private final Throwable throwable;

    public ExceptionDialog(Throwable throwable) {
        super(AlertType.ERROR);
        this.throwable = throwable;
        init();
    }

    private void init() {
        setTitle("Exception");
        setHeaderText(throwable.getMessage());
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        String exceptionText = sw.toString();
        TextArea stacktraceArea = new TextArea(exceptionText);
        stacktraceArea.setEditable(false);
        stacktraceArea.setWrapText(false);
        HBox buttons = new HBox();
        buttons.setAlignment(Pos.CENTER_RIGHT);
        buttons.setPadding(new Insets(5.0));
        Button copyToClipboardButton = new Button("Copy to Clipboard");
        copyToClipboardButton.setOnAction(a -> {
            Map<DataFormat, Object> content = new HashMap<>();
            content.put(DataFormat.PLAIN_TEXT, stacktraceArea.getText());
            Clipboard.getSystemClipboard().setContent(content);
        });
        buttons.getChildren().add(copyToClipboardButton);

        VBox centerBox = new VBox(stacktraceArea, buttons);
        VBox.setVgrow(stacktraceArea, Priority.ALWAYS);
        getDialogPane().setExpandableContent(centerBox);
        stacktraceArea.setPrefSize(900.0, 400.0);
        getDialogPane().expandedProperty().addListener(c -> {
            getDialogPane().getScene().getWindow().centerOnScreen();
        });
    }

}

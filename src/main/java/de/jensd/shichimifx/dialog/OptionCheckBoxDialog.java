package de.jensd.shichimifx.dialog;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters
 */
public class OptionCheckBoxDialog extends Alert {

    private final CheckBox optionCheckBox;

    public OptionCheckBoxDialog(String optionMessage) {
        super(AlertType.CONFIRMATION);
        getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
        optionCheckBox = new CheckBox(optionMessage);
        VBox contentBox = new VBox();
        HBox.setHgrow(contentBox, Priority.ALWAYS);
        contentBox.getChildren().addAll(optionCheckBox);
        contentBox.setSpacing(7.0);
        contentBox.setPrefWidth(500.0);
        getDialogPane().setContent(contentBox);
    }

    public CheckBox getOptionCheckBox() {
        return optionCheckBox;
    }

    public void setOptionSeleted(boolean value) {
        optionCheckBox.setSelected(value);
    }

    public boolean isOptionSelected() {
        return optionCheckBox.isSelected();
    }

    public void setOptionMessage(String message) {
        optionCheckBox.setText(message);
    }

    public void setIcon(String iconUrl) {
        ImageView iconImageView = new ImageView(iconUrl);
        iconImageView.setImage(new Image(iconUrl));
        setGraphic(iconImageView);
    }

}

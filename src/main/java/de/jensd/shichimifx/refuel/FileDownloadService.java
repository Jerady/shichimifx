/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.refuel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 14-01-2015
 */
public class FileDownloadService extends Service<File> {

    private final static Logger log = Logger.getLogger(FileDownloadService.class.getName());
    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private final HttpClient httpClient;
    private final String remoteUrl;
    private ObjectProperty<File> localFile;
    private int bufferSize;

    public FileDownloadService(String remoteURL, File localFile) {
        this.remoteUrl = remoteURL;
        setLocalFile(localFile);
        this.httpClient = HttpClientBuilder.create().build();
        this.bufferSize = DEFAULT_BUFFER_SIZE;
    }

    public ObjectProperty<File> localFileProperty() {
        if (localFile == null) {
            localFile = new SimpleObjectProperty<>();
        }
        return localFile;
    }

    public void setLocalFile(File localFile) {
        this.localFileProperty().setValue(localFile);
    }

    public File getLocalFile() {
        return localFileProperty().getValue();
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    @Override
    protected void succeeded() {
        FileUtils.deleteQuietly(getValue());
    }

    @Override
    protected Task<File> createTask() {
        return new Task<File>() {
            @Override
            protected File call() throws Exception {
                updateMessage(String.format("Start downloading %s to %s", remoteUrl, getLocalFile().getAbsolutePath()));
                HttpGet httpGet = new HttpGet(remoteUrl);
                HttpResponse response = httpClient.execute(httpGet);
                InputStream remoteContentStream = response.getEntity().getContent();
                OutputStream localFileStream = null;
                try {
                    long fileSize = response.getEntity().getContentLength();
                    File dir = getLocalFile().getParentFile();
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    File tempFile = new File(getLocalFile().getAbsolutePath() + ".download");
                    if (tempFile.exists()) {
                        FileUtils.deleteQuietly(tempFile);
                    }
                    localFileStream = new FileOutputStream(tempFile);
                    updateMessage(String.format("Downloading file %s to %s", remoteUrl, tempFile.getAbsolutePath()));
                    byte[] buffer = new byte[bufferSize];
                    int sizeOfChunk;
                    int amountComplete = 0;
                    while ((sizeOfChunk = remoteContentStream.read(buffer)) != -1) {
                        localFileStream.write(buffer, 0, sizeOfChunk);
                        amountComplete += sizeOfChunk;
                        updateProgress(amountComplete, fileSize);
                        updateMessage(String.format("%s/%s", tempFile.getAbsolutePath(), getLocalFile().getAbsolutePath()));
                    }
                    updateMessage(String.format("Download of file %s to %s completed.", remoteUrl, tempFile.getAbsolutePath()));
                    try {
                        if (getLocalFile().exists()) {
                            getLocalFile().delete();
                        }
                        FileUtils.copyFile(tempFile, getLocalFile());
                        updateMessage(String.format("Renaming %s to %s", tempFile.getAbsolutePath(), getLocalFile().getAbsolutePath()));
                    } catch (Exception e) {
                        log.severe(e.getMessage());
                    }
                    return tempFile;
                } finally {
                    remoteContentStream.close();
                    if (localFileStream != null) {
                        localFileStream.close();
                    }
                }
            }
        };
    }
}

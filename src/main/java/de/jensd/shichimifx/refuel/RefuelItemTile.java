/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.refuel;

import de.dimaki.refuel.appcast.entity.Item;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.utils.FontAwesomeIconFactory;
import de.jensd.shichimifx.utils.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 14-01-2015
 */
public class RefuelItemTile extends VBox {

    private ResourceBundle resources;
    @FXML
    private Button cancelButton;
    @FXML
    private Button downloadButton;
    @FXML
    private Button openLocalFileButton;
    @FXML
    private Label titleLabel;
    @FXML
    private Label versionLabel;
    @FXML
    private MenuButton optionsButton;
    @FXML
    private MenuItem changeDownloadLocationMenuItem;
    @FXML
    private Parent progressPane;
    @FXML
    private ProgressIndicator progressIndicator;

    @FXML
    private ProgressBar progressBar;

    private final Item item;
    private final FileDownloadService fileDownloadService;

    public RefuelItemTile(Item item) {
        this.item = item;
        String fileName = FilenameUtils.getName(item.getEnclosure().getUrl());
        String localFile = String.format("%s/%s", System.getProperty("user.home"), fileName);
        fileDownloadService = new FileDownloadService(item.getEnclosure().getUrl(), new File(localFile));
        init();
    }

    private void init() {
        try {
            resources = ResourceBundle.getBundle("i18n/refuel");
            URL fxmlURL = getClass().getResource("RefuelItemTile.fxml");
            FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL, resources);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(RefuelItemTile.class.getName()).log(Level.SEVERE, null, ex);
        }

        titleLabel.setText(item.getTitle());
        versionLabel.setText("Version " + item.getEnclosure().getVersion());
        optionsButton.setText("");
        fileDownloadService.messageProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            System.out.println(newValue);
            progressBar.getTooltip().setText(newValue);
        });
        changeDownloadLocationMenuItem.setOnAction((ActionEvent event) -> {
            onChangeDownloadLocation();
        });

        downloadButton.setOnAction((ActionEvent event) -> {
            fileDownloadService.restart();
        });

        cancelButton.setOnAction((ActionEvent event) -> {
            fileDownloadService.cancel();
        });

        openLocalFileButton.setOnAction((ActionEvent event) -> {
            onOpenFile();
        });

        downloadButton.disableProperty().bind(fileDownloadService.runningProperty());
        optionsButton.disableProperty().bind(fileDownloadService.runningProperty());
        openLocalFileButton.disableProperty().bind(fileDownloadService.runningProperty());
        progressPane.visibleProperty().bind(fileDownloadService.runningProperty());
        progressBar.progressProperty().bind(fileDownloadService.progressProperty());
        progressIndicator.setProgress(-1.0);
        updateOpenFileButtonState();
        fileDownloadService.localFileProperty().addListener((Observable observable) -> {
            updateOpenFileButtonState();
        });
        fileDownloadService.setOnSucceeded((WorkerStateEvent event) -> {
            updateOpenFileButtonState();
        });

    }

    private void updateOpenFileButtonState() {
        openLocalFileButton.setVisible(fileDownloadService.getLocalFile() != null && fileDownloadService.getLocalFile().exists());
    }

    private void onChangeDownloadLocation() {
        FileChooser chooser = new FileChooser();
        chooser.setInitialFileName(fileDownloadService.getLocalFile().getName());
        File localFile = chooser.showSaveDialog(this.getScene().getWindow());
        if (localFile != null) {
            fileDownloadService.setLocalFile(localFile);
        }
    }

    private void onOpenFile() {
        Desktop.edit(fileDownloadService.getLocalFile().getAbsolutePath());
        Platform.exit();
    }
}

/* 
 * Copyright 2015 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.refuel;

import de.dimaki.refuel.appcast.entity.Appcast;
import de.dimaki.refuel.appcast.entity.Item;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 14-01-2015
 */
public class UpdatePane extends VBox {

    private final Appcast appcast;
    private final String currentVersion;
    @FXML
    private Label currentVersionLabel;
    @FXML
    private ListView<Item> availablePackagesListView;
    @FXML
    private WebView releaseNotesWebView;

    private ObservableList<Item> availablePackagesList;
    private BooleanProperty checkUpdatesOnAppStart;
    private ResourceBundle resources;

    public UpdatePane(String currentVersion, Appcast appcast) {
        this.currentVersion = currentVersion;
        this.appcast = appcast;
        init();
    }

    private void init() {
        try {
            resources = ResourceBundle.getBundle("i18n/refuel");
            URL fxmlURL = getClass().getResource("UpdatePane.fxml");
            FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL, resources);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(UpdatePane.class.getName()).log(Level.SEVERE, null, ex);
        }

        availablePackagesList = FXCollections.observableArrayList();
        availablePackagesListView.setItems(availablePackagesList);
        availablePackagesListView.setCellFactory((ListView<Item> param) -> {
            return new ItemListCell();
        });
        availablePackagesList.setAll(appcast.getChannel().getItems());

        currentVersionLabel.setText(currentVersion);

        WebEngine engine = releaseNotesWebView.getEngine();
        engine.load(appcast.getChannel().getItems().get(0).getReleaseNotesLink());
    }


    public BooleanProperty checkUpdatesOnAppStartProperty() {
        if (checkUpdatesOnAppStart == null) {
            checkUpdatesOnAppStart = new SimpleBooleanProperty();
        }
        return checkUpdatesOnAppStart;
    }

    private class ItemListCell extends ListCell<Item> {

        public ItemListCell() {
            getStyleClass().addAll("refuel-item-list-cell");
        }

        @Override
        protected void updateItem(Item item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                RefuelItemTile itemTile = new RefuelItemTile(item);
                setGraphic(itemTile);
                setText(null);
            } else {
                setText(null);
                setGraphic(null);
            }
        }
    }

}

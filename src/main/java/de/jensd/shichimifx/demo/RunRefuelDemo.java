/* 
 * Copyright 2014 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.demo;

import de.dimaki.refuel.updater.boundary.Updater;
import de.dimaki.refuel.updater.entity.ApplicationStatus;
import de.jensd.shichimifx.refuel.UpdatePane;
import de.jensd.shichimifx.dialog.OptionCheckBoxDialog;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Optional;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Jens Deters (www.jensd.de)
 * @version 1.0.0
 * @since 13-01-2015
 */
public class RunRefuelDemo extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Updater updater = new Updater();
        String currentVersion = "0.0.12";
        ApplicationStatus applicationStatus = updater.getApplicationStatus(currentVersion, updateURL().get());
        if (ApplicationStatus.UPDATE_AVAILABLE.equals(applicationStatus)) {
            String messageTitle = "Updates available for MQTT.fx";
            String message = "Would you like to have a look?";
            String optionMessage = "Check for updates on application start.";
            OptionCheckBoxDialog dialog = new OptionCheckBoxDialog(optionMessage );
            dialog.setHeaderText(messageTitle);
            dialog.setContentText(message);
            dialog.setOptionSeleted(true);
            Optional<ButtonType> response = dialog.showAndWait();
            if (response.get() == ButtonType.YES) {
                UpdatePane updateDownloadPane = new UpdatePane(currentVersion, applicationStatus.getAppcast());
                Scene scene = new Scene(updateDownloadPane);
                scene.getStylesheets().add("/styles/shichimifx.css");
                Stage stage = new Stage(StageStyle.UNIFIED);
                stage.setScene(scene);
                stage.show();
            }
        }
    }

    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        launch(args);
    }

    public Optional<URL> updateURL() {
        URL url = null;
        try {
            url = new URL("http://www.jensd.de/apps/mqttfx/appcast.xml");
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        return Optional.of(url);
    }

}

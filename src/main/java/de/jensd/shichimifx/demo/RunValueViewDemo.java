/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.demo;

import de.jensd.shichimifx.control.ValueView;
import de.jensd.fx.glyphs.weathericons.WeatherIcon;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class RunValueViewDemo extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        VBox root = new VBox();

        ValueView valueView = new ValueView();
        valueView.setGlyphName(WeatherIcon.DAY_FOG.name());
        valueView.setValue("37.4");
        valueView.setUnit("\u2103");
        valueView.setFontSize("2em");
        valueView.setGlyphSize(48.0);
        valueView.setTitle("Temperature Outdoor");
        
        VBox.setVgrow(valueView, Priority.ALWAYS);
        root.getChildren().add(valueView);

        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

package de.jensd.shichimifx.demo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

public class RunOutputConsoleDemo extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/OutputConsoleDemo.fxml"));
        Scene scene = new Scene(root, 700, 500);
//        scene.getStylesheets().add("/styles/shichimifx.css");
        stage.setTitle("OutputConsole // Demo");
        stage.setScene(scene);
        stage.show();

    }

}

package de.jensd.shichimifx.demo;

import de.jensd.shichimifx.dialog.Dialogs;
import de.jensd.shichimifx.dialog.OptionCheckBoxDialog;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Optional;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class RunDialogsDemo extends Application {

    @Override
    public void start(Stage primaryStage) {
        Button btnEx = new Button();
        btnEx.setText("Show Exception Dialog");
        btnEx.setOnAction((ActionEvent event) -> {
            Exception ex = new FileNotFoundException("Could not fing keystore file: private_keystore.jks");
            Dialogs.showException(ex);
        });

        Button btnChoice = new Button();
        btnChoice.setText("Show Checkbox Choice Dialog");
        btnChoice.setOnAction((ActionEvent event) -> {
            String messageTitle = "Exit the Application?";
            String optionMessage = "Don't ask me again.";
            OptionCheckBoxDialog dialog = new OptionCheckBoxDialog(optionMessage);
            dialog.setHeaderText(messageTitle);
            dialog.setIcon("/images/MQTT.fx-icon_48.png");
            dialog.initOwner(primaryStage);
            dialog.initModality(Modality.APPLICATION_MODAL);
            Optional<ButtonType> response = dialog.showAndWait();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Your Choice:");
            alert.setContentText(response.get().getText() + " & Don't ask again: " + dialog.isOptionSelected());
            alert.showAndWait();
        });
        
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(5.0);
        root.getChildren().addAll(btnEx, btnChoice);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        
        launch(args);
    }

}

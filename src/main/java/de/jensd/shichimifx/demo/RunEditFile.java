package de.jensd.shichimifx.demo;

import de.jensd.shichimifx.utils.Desktop;

/**
 *
 * @author Jens Deters
 */
public class RunEditFile {

    public static void main(String[] args) {
        Desktop.edit("http://www.heise.de");
    }

}

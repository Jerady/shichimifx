package de.jensd.shichimifx.demo;

import de.jensd.shichimifx.utils.Shell;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jens Deters
 */
public class RunShellExec {

    public static void main(String[] args) {
        try {
            Process p = Shell.execute("ls", "-la").get();
            p.waitFor();
            String line;
            try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
                while ((line = input.readLine()) != null) {
                    System.out.println(line);
                }
            }
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(RunShellExec.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

/* 
 * Copyright 2017 Jens Deters.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.jensd.shichimifx.demo;

import de.jensd.shichimifx.control.DeviceSwitch;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters (mail@jensd.de)
 */
public class RunDeviceSwitchDemo extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        VBox root = new VBox();
        root.setSpacing(5);
        root.setPadding(new Insets(10));
        
        DeviceSwitch deviceSwitch1 = new DeviceSwitch();
        deviceSwitch1.setTitle("Licht Wohnzimmer");
        deviceSwitch1.setOnSwitchOn(e -> {
            System.out.println("ON");
        });
        deviceSwitch1.setOnSwitchOff(e -> {
            System.out.println("OFF");
        });

        DeviceSwitch deviceSwitch2 = new DeviceSwitch();
        DeviceSwitch deviceSwitch3 = new DeviceSwitch();
        DeviceSwitch deviceSwitch4 = new DeviceSwitch();

        deviceSwitch2.setTitle("Licht Terrasse");
        deviceSwitch3.setTitle("Licht Haustür");
        deviceSwitch4.setTitle("Licht Garten");

        root.getChildren().addAll(deviceSwitch1, deviceSwitch2, deviceSwitch3, deviceSwitch4);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
